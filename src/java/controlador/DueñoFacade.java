/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import entidades.Dueño;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Paul Alexander Posada
 * @teamWork
 * @version 1.0
 * @date 2/03/2018
 * @time 05:57:29 PM
 * 
 */
@Stateless
public class DueñoFacade extends AbstractFacade<Dueño> {

    @PersistenceContext(unitName = "bodePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DueñoFacade() {
        super(Dueño.class);
    }

}
