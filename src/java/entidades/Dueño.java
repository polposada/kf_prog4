/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Paul Alexander Posada
 * @teamWork
 * @version 1.0
 * @date 2/03/2018
 * @time 05:57:10 PM
 * 
 */
@Entity
@Table(name = "due\u00f1o")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Due\u00f1o.findAll", query = "SELECT d FROM Due\u00f1o d")
    , @NamedQuery(name = "Due\u00f1o.findByPkDue\u00f1o", query = "SELECT d FROM Due\u00f1o d WHERE d.pkDue\u00f1o = :pkDue\u00f1o")
    , @NamedQuery(name = "Due\u00f1o.findByNombre", query = "SELECT d FROM Due\u00f1o d WHERE d.nombre = :nombre")})
public class Dueño implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "pk_due\u00f1o")
    private Integer pkDueño;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 32)
    @Column(name = "nombre")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkDue\u00f1o")
    private List<Maletin> maletinList;

    public Dueño() {
    }

    public Dueño(Integer pkDueño) {
        this.pkDueño = pkDueño;
    }

    public Dueño(Integer pkDueño, String nombre) {
        this.pkDueño = pkDueño;
        this.nombre = nombre;
    }

    public Integer getPkDueño() {
        return pkDueño;
    }

    public void setPkDueño(Integer pkDueño) {
        this.pkDueño = pkDueño;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public List<Maletin> getMaletinList() {
        return maletinList;
    }

    public void setMaletinList(List<Maletin> maletinList) {
        this.maletinList = maletinList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkDueño != null ? pkDueño.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dueño)) {
            return false;
        }
        Dueño other = (Dueño) object;
        if ((this.pkDueño == null && other.pkDueño != null) || (this.pkDueño != null && !this.pkDueño.equals(other.pkDueño))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Due\u00f1o[ pkDue\u00f1o=" + pkDueño + " ]";
    }

}
