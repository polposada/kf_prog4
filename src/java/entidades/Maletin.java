/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paul Alexander Posada
 * @teamWork
 * @version 1.0
 * @date 2/03/2018
 * @time 05:57:10 PM
 * 
 */
@Entity
@Table(name = "maletin")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Maletin.findAll", query = "SELECT m FROM Maletin m")
    , @NamedQuery(name = "Maletin.findByPkMaletin", query = "SELECT m FROM Maletin m WHERE m.pkMaletin = :pkMaletin")
    , @NamedQuery(name = "Maletin.findByContenido", query = "SELECT m FROM Maletin m WHERE m.contenido = :contenido")})
public class Maletin implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "pk_maletin")
    private Integer pkMaletin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "contenido")
    private String contenido;
    @JoinColumn(name = "fk_due\u00f1o", referencedColumnName = "pk_due\u00f1o")
    @ManyToOne(optional = false)
    private Dueño fkDueño;
    @JoinColumn(name = "fk_edificio", referencedColumnName = "fk_edificio")
    @ManyToOne(optional = false)
    private Edificio fkEdificio;

    public Maletin() {
    }

    public Maletin(Integer pkMaletin) {
        this.pkMaletin = pkMaletin;
    }

    public Maletin(Integer pkMaletin, String contenido) {
        this.pkMaletin = pkMaletin;
        this.contenido = contenido;
    }

    public Integer getPkMaletin() {
        return pkMaletin;
    }

    public void setPkMaletin(Integer pkMaletin) {
        this.pkMaletin = pkMaletin;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public Dueño getFkDueño() {
        return fkDueño;
    }

    public void setFkDueño(Dueño fkDueño) {
        this.fkDueño = fkDueño;
    }

    public Edificio getFkEdificio() {
        return fkEdificio;
    }

    public void setFkEdificio(Edificio fkEdificio) {
        this.fkEdificio = fkEdificio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkMaletin != null ? pkMaletin.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Maletin)) {
            return false;
        }
        Maletin other = (Maletin) object;
        if ((this.pkMaletin == null && other.pkMaletin != null) || (this.pkMaletin != null && !this.pkMaletin.equals(other.pkMaletin))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Maletin[ pkMaletin=" + pkMaletin + " ]";
    }

}
