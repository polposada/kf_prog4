/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Paul Alexander Posada
 * @teamWork
 * @version 1.0
 * @date 2/03/2018
 * @time 05:57:11 PM
 * 
 */
@Entity
@Table(name = "edificio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Edificio.findAll", query = "SELECT e FROM Edificio e")
    , @NamedQuery(name = "Edificio.findByFkEdificio", query = "SELECT e FROM Edificio e WHERE e.fkEdificio = :fkEdificio")
    , @NamedQuery(name = "Edificio.findByFkCiudad", query = "SELECT e FROM Edificio e WHERE e.fkCiudad = :fkCiudad")
    , @NamedQuery(name = "Edificio.findByCapacidad", query = "SELECT e FROM Edificio e WHERE e.capacidad = :capacidad")
    , @NamedQuery(name = "Edificio.findByCodigoPostal", query = "SELECT e FROM Edificio e WHERE e.codigoPostal = :codigoPostal")})
public class Edificio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "fk_edificio")
    private Integer fkEdificio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fk_ciudad")
    private int fkCiudad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "capacidad")
    private double capacidad;
    @Size(max = 10)
    @Column(name = "codigoPostal")
    private String codigoPostal;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkEdificio")
    private List<Maletin> maletinList;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "edificio1")
    private Edificio edificio;
    @JoinColumn(name = "fk_edificio", referencedColumnName = "fk_edificio", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Edificio edificio1;

    public Edificio() {
    }

    public Edificio(Integer fkEdificio) {
        this.fkEdificio = fkEdificio;
    }

    public Edificio(Integer fkEdificio, int fkCiudad, double capacidad) {
        this.fkEdificio = fkEdificio;
        this.fkCiudad = fkCiudad;
        this.capacidad = capacidad;
    }

    public Integer getFkEdificio() {
        return fkEdificio;
    }

    public void setFkEdificio(Integer fkEdificio) {
        this.fkEdificio = fkEdificio;
    }

    public int getFkCiudad() {
        return fkCiudad;
    }

    public void setFkCiudad(int fkCiudad) {
        this.fkCiudad = fkCiudad;
    }

    public double getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(double capacidad) {
        this.capacidad = capacidad;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    @XmlTransient
    public List<Maletin> getMaletinList() {
        return maletinList;
    }

    public void setMaletinList(List<Maletin> maletinList) {
        this.maletinList = maletinList;
    }

    public Edificio getEdificio() {
        return edificio;
    }

    public void setEdificio(Edificio edificio) {
        this.edificio = edificio;
    }

    public Edificio getEdificio1() {
        return edificio1;
    }

    public void setEdificio1(Edificio edificio1) {
        this.edificio1 = edificio1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (fkEdificio != null ? fkEdificio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Edificio)) {
            return false;
        }
        Edificio other = (Edificio) object;
        if ((this.fkEdificio == null && other.fkEdificio != null) || (this.fkEdificio != null && !this.fkEdificio.equals(other.fkEdificio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidades.Edificio[ fkEdificio=" + fkEdificio + " ]";
    }

}
